package com.reactnativeanalysisexample;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.util.Log;

import androidx.core.app.ActivityCompat;

import com.facebook.react.bridge.NativeModule;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import java.util.Map;
import java.util.HashMap;

import getpoi.com.poibeaconsdk.PoiScanner;

public class PoilabsAnalysisModule extends ReactContextBaseJavaModule {
    PoilabsAnalysisModule(ReactApplicationContext context) {
        super(context);
        this.context = context;
    }
    ReactApplicationContext context;
    private String appId="appid";
    private String secretId="secret";
    private String uniqueId="uniqueId";

    private final int REQUEST_FOREGROUND_AND_BACKGROUND_REQUEST_CODE = 56;
    private final int REQUEST_COARSE_LOCATION = 57;

    @Override
    public String getName() {
        return "PoilabsAnalysisModule";
    }

    @ReactMethod
    public void startPoilabsAnalysis() {
        Log.d("PoilabsAnalysisModule", "startPoilabsAnalysis method is called");

        PoiScanner.Config config = new PoiScanner.Config(secretId,uniqueId,appId);
        config.setEnabled(true);
        config.setOpenSystemBluetooth(false);

        PoiScanner.init(config, this.context);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            askLocalPermission();
        } else {
            startScan();
        }
    }

    private void startScan() {
        PoiScanner.startScan(this.context);

        PoiScanner.bind(new PoiScanner.PoiResponseListener() {
            @Override
            public void onResponse(String s) {
                Log.d("PoilabsAnalysisModule", s);
            }

            @Override
            public void onFail(Exception e) {
                Log.d("PoilabsAnalysisModule", e.toString());
            }
        });
    }

    private void askLocalPermission() {
        if (Build.VERSION.SDK_INT >= 29) {
            int hasFineLocationPerm=ActivityCompat.checkSelfPermission(getCurrentActivity(), Manifest.permission.ACCESS_FINE_LOCATION);
            int hasBackgroundLocationPerm=ActivityCompat.checkSelfPermission(getCurrentActivity(), Manifest.permission.ACCESS_BACKGROUND_LOCATION);
            if(hasFineLocationPerm!=PackageManager.PERMISSION_GRANTED && hasBackgroundLocationPerm!=PackageManager.PERMISSION_GRANTED){
                ActivityCompat.requestPermissions(getCurrentActivity(),new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_BACKGROUND_LOCATION},
                        REQUEST_FOREGROUND_AND_BACKGROUND_REQUEST_CODE);
            }else{
                startScan();
            }
        }else{
            int hasCourseLocation=ActivityCompat.checkSelfPermission(getCurrentActivity(), Manifest.permission.ACCESS_COARSE_LOCATION);
            if(hasCourseLocation!=PackageManager.PERMISSION_GRANTED){
                ActivityCompat.requestPermissions(getCurrentActivity(), new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, REQUEST_COARSE_LOCATION);
            }else{
                startScan();
            }
        }
    }

}
